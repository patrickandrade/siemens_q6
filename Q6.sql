DROP TABLE IF EXISTS Students;
CREATE TABLE Students ( ID INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Value INTEGER);
INSERT INTO Students
(Name, Value) 
VALUES 
('Plinio', 		52),
('Fernando', 	71),
('Prisicila', 	48),
('Patrick', 	85),
('Rafael', 		92),
('Rafaela', 	91),
('Pedro', 		96),
('Andrade', 	63),
('Jonas', 		65),
('Alex', 		69);

DROP TABLE IF EXISTS Notes;
CREATE TABLE Notes ( Grade Integer, Min_Value InTEGER, Max_Value INTEGER);
INSERT INTO Notes
(Grade, Min_Value, Max_Value) 
VALUES 
(1, 	0, 	9),
(2, 	10, 19),
(3, 	20, 29),
(4, 	30, 39),
(5, 	40, 49),
(6, 	50, 59),
(7, 	60, 69),
(8, 	70, 79),
(9, 	80, 89),
(10, 	90, 100);

SELECT Name, Grade, Value FROM
(
SELECT Name, Grade, Value FROM Students JOIN Notes ON 
Students.Value >= Notes.Min_Value and 
Students.Value <= Notes.Max_Value WHERE 
Notes.Grade >= 8 order by Grade DESC, Name ASC
)
UNION ALL
SELECT 'NULL', Grade, Value FROM Students JOIN Notes ON 
Students.Value >= Notes.Min_Value and 
Students.Value <= Notes.Max_Value WHERE 
Notes.Grade <=7 ORDER BY Grade DESC, Grade ASC;